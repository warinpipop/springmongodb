package com.test.springbootmongodb.service;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.mongodb.Function;
import com.test.springbootmongodb.User.UserModel;
import com.test.springbootmongodb.User.UserRepository;
import com.test.springbootmongodb.User.Userservice;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
@Category(Function.class)
public class UserServiceTest {
    @InjectMocks
    Userservice service = new Userservice();

    @Mock
    private UserRepository repository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findCustomerAllShoulrFoundData() throws Exception {
        UserModel user = new UserModel();

        user.setId("id-01");
        user.setName("name");
        user.setBirthDate(new Date());
        UserModel user1 = new UserModel();
        user1.setId("id-02");
        user1.setName("art");
        user1.setBirthDate(new Date());

        List<UserModel> listUserModel = new ArrayList<>();
        listUserModel.add(user);
        listUserModel.add(user1);

        Mockito.when(repository.findAll()).thenReturn(listUserModel);
        List<UserModel> result = service.findUserAll();
        assertEquals(listUserModel.size(), result.size());
        verify(repository).findAll();
    }

    @Test
    public void findCustomerByIdShoulrFoundData() throws Exception {
        UserModel user = new UserModel();
        user.setId("id-01");
        user.setName("name");
        user.setBirthDate(new Date());

        Mockito.when(repository.findById("id-01")).thenReturn(Optional.of(user));
        UserModel result = service.findUserById("id-01");
        assertEquals(user.getId(), result.getId());
        assertEquals(user.getName(), result.getName());
        assertEquals(user.getBirthDate(), result.getBirthDate());
        verify(repository).findById(any(String.class));

    }

    @Test
    public void whenCreateUserShouldReturnData() throws Exception {
        UserModel user = new UserModel();
        user.setId("id-01");
        user.setName("name");
        user.setBirthDate(new Date());

        Mockito.when(repository.save(user)).thenReturn(user);
        UserModel result = service.createUserModel(user);
        assertEquals(user.getId(), result.getId());
        assertEquals(user.getName(), result.getName());
        assertEquals(user.getBirthDate(), result.getBirthDate());
        verify(repository).save(any(UserModel.class));

    }

    @Test
    public void UpdateUserShouldUpdateUser() throws Exception {
        UserModel user = new UserModel();
        user.setId("id-01");
        user.setName("name");
        user.setBirthDate(new Date());
        Mockito.when(repository.findById("id-01")).thenReturn(Optional.of(user));
        Mockito.when(repository.save(any())).thenReturn(user);
        System.out.println(user);

        UserModel result = service.updateUser("id-01", user);
        System.out.println(result);
        assertEquals(user.getId(), result.getId());
        assertEquals(user.getName(), result.getName());
        assertEquals(user.getBirthDate(), result.getBirthDate());

        verify(repository).save(any(UserModel.class));

    }

    @Test
    public void deleteUserById() throws Exception {

        UserModel user = new UserModel();
        user.setId("id_01");
        user.setName("account");
        user.setBirthDate(new Date());

        service.deleteById(user.getId());
        verify(repository).deleteById(any(String.class));

    }

}