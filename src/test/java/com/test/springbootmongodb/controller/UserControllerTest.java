package com.test.springbootmongodb.controller;

import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.springbootmongodb.User.UserController;
import com.test.springbootmongodb.User.UserModel;
import com.test.springbootmongodb.User.UserRepository;
import com.test.springbootmongodb.User.Userservice;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(SpringRunner.class)

public class UserControllerTest {

    @InjectMocks
    UserController controller = new UserController();
    @Mock
    private Userservice service;

    @Mock
    private UserRepository repository;

    // private ObjectMapper objectMapper = new ObjectMapper();

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

    }

    @Test
    public void getAllUserShouldReturnListUser() throws Exception {
        UserModel user = new UserModel();
        user.setId("id-1");
        user.setName("name");
        user.setBirthDate(new Date());

        UserModel user1 = new UserModel();
        user1.setId("id-2");
        user1.setName("name1");
        user1.setBirthDate(new Date());

        List<UserModel> listUserModel = new ArrayList<>();
        listUserModel.add(user);
        listUserModel.add(user1);

        Mockito.when(service.findUserAll()).thenReturn(listUserModel);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/users")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").value(user.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(user.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].birthDate").value(user.getBirthDate()));
    }

    @Test
    public void getUserByIdShouldReturnUser() throws Exception {
        UserModel user = new UserModel();
        user.setId("1");
        user.setName("name");
        user.setBirthDate(new Date());

        Mockito.when(service.findUserById(any())).thenReturn(user);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/users/1")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(user.getId()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(user.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.birthDate").value(user.getBirthDate()));
    }

    @Test
    public void getUserByIdWithBodyWrongShouldReturnStatusNotFound() throws Exception {
        UserModel user = new UserModel();
        user.setId("1");
        user.setName("name");
        user.setBirthDate(new Date());

        Mockito.when(service.findUserById("1")).thenReturn(user);
        this.mockMvc.perform(MockMvcRequestBuilders.get("/users/2")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());

    }

    @Test
    public void createUserWithBodyIsWrongShouldReturnStatusBadRequest() throws Exception {

        String body = "Bad Body";

        this.mockMvc
                .perform(MockMvcRequestBuilders.post("/users/create").contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void createUserShouldReturnStatusCreaterRequset() throws Exception {

        UserModel user = new UserModel();
        user.setName("name");
        user.setBirthDate(new Date());

        String body = "{\"name\":\"name1\" , \"birthDate\":\"2018-05-03T02:58:44.615+0000\"}";

        Mockito.when(service.createUserModel(any(UserModel.class))).thenReturn(user);

        this.mockMvc
                .perform(MockMvcRequestBuilders.post("/users/create").contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(user.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.birthDate").value(user.getBirthDate()));

    }

    @Test
    public void createUserWithBodyNullShouldReturnStatusBadRequest() throws Exception {

        String body = "{\"name\":\"\"}";

        this.mockMvc
                .perform(MockMvcRequestBuilders.post("/users/create").contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andDo(MockMvcResultHandlers.print()).andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void updateUserShouldReturnStatusOkRequest() throws Exception {
        UserModel user = new UserModel();
        user.setId("1");
        user.setName("name");
        user.setBirthDate(new Date());
        UserModel newUser = new UserModel();
        newUser.setId("1");
        newUser.setName("NEWname");
        newUser.setBirthDate(new Date());

        Mockito.when(service.updateUser(any(), any())).thenReturn(newUser);

        String body = "{\"name\":\"NEWname\", \"birthDate\":\"2018-05-03T02:58:44.615+0000\"}";

        this.mockMvc
                .perform(MockMvcRequestBuilders.put("/users/update/1").contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andDo(MockMvcResultHandlers.print()).andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value(newUser.getName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.birthDate").value(newUser.getBirthDate()));
    }

    @Test
    public void updateUserWithBodyIsWrongShouldReturnStatusBadRequest() throws Exception {
        UserModel user = new UserModel();
        user.setId("1");
        user.setName("name");
        user.setBirthDate(new Date());
        String body = "Bad Request";
        Mockito.when(service.updateUser(any(), any())).thenReturn(user);
        this.mockMvc
                .perform(MockMvcRequestBuilders.put("/users/update/1").contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andDo(MockMvcResultHandlers.print()).andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void updateUserWithBodyNamelessThenTwoShouldReturnStatusBadRequest() throws Exception {
        UserModel user = new UserModel();
        user.setId("1");
        user.setName("n");
        user.setBirthDate(new Date());

        String body = "{\"name\":\"N\", \"birthDate\":\"2018-05-03T02:58:44.615+0000\"}";
        Mockito.when(service.updateUser(any(), any())).thenReturn(user);
        this.mockMvc
                .perform(MockMvcRequestBuilders.put("/users/update/1").contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andDo(MockMvcResultHandlers.print()).andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void updateUserWithBodyNameNullNotShouldReturnStatusBadRequest() throws Exception {
        UserModel user = new UserModel();
        user.setId("1");
        user.setName("");
        user.setBirthDate(new Date());
        String body = "{\"name\":\"\", \"birthDate\":\"2018-05-03T02:58:44.615+0000\"}";
        Mockito.when(service.updateUser(any(), any())).thenReturn(user);

        this.mockMvc
                .perform(MockMvcRequestBuilders.put("/users/update/1").contentType(MediaType.APPLICATION_JSON)
                        .content(body))
                .andDo(MockMvcResultHandlers.print()).andDo(MockMvcResultHandlers.log())
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void deleteUserShouldReturnStatusOK() throws Exception {
        UserModel user = new UserModel();

        Mockito.when(service.findUserById("1")).thenReturn(user);
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/users/delete/1")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteUserWithNotIdShouldReturnStatusNotFound() throws Exception {
        UserModel user = new UserModel();
        user.setId("1");
        user.setName("Art");
        user.setBirthDate(new Date());

        Mockito.when(service.findUserById("1")).thenReturn(user);
        this.mockMvc.perform(MockMvcRequestBuilders.delete("/users/delete/2")).andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}