package com.test.springbootmongodb.User;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<UserModel, String> {
    Optional<UserModel> findByName(String name);

    Optional<UserModel> findCustomerById(String id);

    Optional<UserModel> findById(String id);

    void deleteById(String id);
}