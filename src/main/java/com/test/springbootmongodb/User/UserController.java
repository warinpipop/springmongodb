package com.test.springbootmongodb.User;

import java.util.List;

import javax.validation.Valid;

import com.test.springbootmongodb.Exception.UserNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "*")
@RestController
public class UserController {
    private static final String USER_NOT_FOUND_BODY_MESSAGE = "User not found.";

    @Autowired
    private Userservice service;

    @CrossOrigin(origins = "http://localhost:8080")
    @GetMapping(path = "/test")
    public String testServer() {
        return "Test:Server";
    }

    @GetMapping(path = "/users")
    public List<UserModel> getAllUser() {
        System.out.println("######USERS LIST AT BEGINNING######");
        System.out.println(service.findUserAll());
        System.out.println("###################################\n\n");
        return service.findUserAll();

    }

    @PostMapping(path = "/users/create")
    public ResponseEntity<UserModel> PostUser(@Valid @RequestBody UserModel user) {
        UserModel users = service.createUserModel(user);
        System.out.println("User:" + users);

        return ResponseEntity.status(HttpStatus.CREATED).body(users);

    }

    @GetMapping(path = "/users/{id}")
    public ResponseEntity<?> getFindOneId(@Valid @PathVariable String id) {
        UserModel userOptional = service.findUserById(id);
        System.out.println("######USERS GET BY ID######");
        System.out.println(userOptional);
        System.out.println("###################################\n\n");
        if (userOptional == null) {
            throw new UserNotFoundException(USER_NOT_FOUND_BODY_MESSAGE);
        }
        return ResponseEntity.ok(userOptional);
    }

    @DeleteMapping(path = "/users/delete/{id}")
    public ResponseEntity<UserModel> deleteById(@PathVariable String id) {
        UserModel userFound = service.findUserById(id);
        System.out.println("######USERS Delete By Id######");
        System.out.println(userFound);
        System.out.println("###################################\n\n");

        if (userFound == null) {
            throw new UserNotFoundException(USER_NOT_FOUND_BODY_MESSAGE);
        }
        service.deleteById(id);
        return ResponseEntity.ok().build();

    }

    @PutMapping(path = "/users/update/{id}")
    public ResponseEntity<?> update(@PathVariable String id, @RequestBody @Validated UserModel user) {
        UserModel userOptional = service.updateUser(id, user);

        return ResponseEntity.ok(userOptional);
    }

}