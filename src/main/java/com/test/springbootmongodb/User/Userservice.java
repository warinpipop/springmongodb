package com.test.springbootmongodb.User;

import java.util.List;
import java.util.Optional;

import com.test.springbootmongodb.Exception.UserNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class Userservice {
    @Autowired
    private UserRepository repository;

    private static final String ACCOUNT_NOT_FOUND_BODY_MESSAGE = "User not found.";

    @Transactional
    public UserModel findUserById(String id) {
        Optional<UserModel> accountOptional = repository.findById(id);
        return accountOptional.orElse(null);
    }

    @Transactional
    public List<UserModel> findUserAll() {
        return repository.findAll();
    }

    public UserModel updateUser(String id, UserModel user) {

        Optional<UserModel> userOptional = repository.findById(id);
        if (!userOptional.isPresent()) {
            throw new UserNotFoundException(ACCOUNT_NOT_FOUND_BODY_MESSAGE);
        }
        user.setId(id);
        UserModel users = repository.save(user);
        return users;
    }

    public void deleteById(String id) {
        repository.deleteById(id);
    }

    public UserModel createUserModel(UserModel user) {
        return repository.save(user);
    }

}