package com.test.springbootmongodb.Customer;

import java.util.List;
import java.util.Optional;

import com.test.springbootmongodb.Exception.UserNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@Controller
@RestController
public class CustomerController {
    private static final String ACCOUNT_NOT_FOUND_BODY_MESSAGE = "User not found.";
    @Autowired
    private CustomerRepository repository;

    @GetMapping(path = "/customers")
    public List<CustomerModel> getCustomerAll() {
        return repository.findAll();
    }

    @PostMapping(path = "/customers/create")
    public ResponseEntity<CustomerModel> postCustomerModel(@RequestBody CustomerModel customer) {
        CustomerModel customers = repository.save(customer);
        return ResponseEntity.status(HttpStatus.CREATED).body(customers);
    }

    @DeleteMapping(path = "/customers/delete/{id}")
    public void deleteCustomerModel(@PathVariable String id) {
        Optional<CustomerModel> customers = repository.findById(id);
        repository.deleteById(id);
        if (customers == null) {
            throw new UserNotFoundException(ACCOUNT_NOT_FOUND_BODY_MESSAGE);
        }
        repository.deleteById(id);

    }

    @PutMapping(path = "/customers/update/{id}")
    public ResponseEntity<CustomerModel> updateCustomerModel(@PathVariable String id,
            @RequestBody @Validated CustomerModel customer) {
        Optional<CustomerModel> studentOptional = repository.findById(id);
        if (!studentOptional.isPresent())
            throw new UserNotFoundException(ACCOUNT_NOT_FOUND_BODY_MESSAGE);
        customer.setId(id);
        CustomerModel customers = repository.save(customer);
        return ResponseEntity.ok(customers);
    }

}