package com.test.springbootmongodb.Customer;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;

public class CustomerModel {
    @Id
    private String id;
    @NotNull
    private String position;
    @NotNull
    private String name;
    private Integer age;
    private String address;
    private Date timeStamp;

    public CustomerModel() {

    }

    public CustomerModel(String id, String position, String name, Integer age, String address, Date timeStamp) {
        super();
        this.id = id;
        this.position = position;
        this.name = name;
        this.age = age;
        this.address = address;
        this.timeStamp = timeStamp;
    }

    public String getId() {
        return id;
    }

    public String getPosition() {
        return position;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    public Object size() {
        return null;
    }
}